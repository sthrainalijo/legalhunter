﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LegalHunter.Migrations
{
    public partial class ChangeQuestionTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExamDesc",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "ExamDuration",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "Question",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.AddColumn<string>(
                name: "CorrectOption",
                table: "ExamQuestionsMapTbl",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsOptionANeedImg",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOptionBNeedImg",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOptionCNeedImg",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOptionDNeedImg",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOptionENeedImg",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "OptionB",
                table: "ExamQuestionsMapTbl",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptionC",
                table: "ExamQuestionsMapTbl",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptionD",
                table: "ExamQuestionsMapTbl",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptionE",
                table: "ExamQuestionsMapTbl",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuestionDesc",
                table: "ExamQuestionsMapTbl",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuestionTitle",
                table: "ExamQuestionsMapTbl",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CorrectOption",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "IsOptionANeedImg",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "IsOptionBNeedImg",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "IsOptionCNeedImg",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "IsOptionDNeedImg",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "IsOptionENeedImg",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "OptionB",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "OptionC",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "OptionD",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "OptionE",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "QuestionDesc",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "QuestionTitle",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.AddColumn<string>(
                name: "ExamDesc",
                table: "ExamQuestionsMapTbl",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "ExamDuration",
                table: "ExamQuestionsMapTbl",
                type: "time",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "Question",
                table: "ExamQuestionsMapTbl",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
