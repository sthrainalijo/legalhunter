﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LegalHunter.Migrations
{
    public partial class ChangeQuestionTbl1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExamQuestionsMapTbl",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "ExamQuestionsMapID",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.AddColumn<int>(
                name: "ExamQuestionID",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExamQuestionsMapTbl",
                table: "ExamQuestionsMapTbl",
                column: "ExamQuestionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExamQuestionsMapTbl",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "ExamQuestionID",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.AddColumn<int>(
                name: "ExamQuestionsMapID",
                table: "ExamQuestionsMapTbl",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExamQuestionsMapTbl",
                table: "ExamQuestionsMapTbl",
                column: "ExamQuestionsMapID");
        }
    }
}
