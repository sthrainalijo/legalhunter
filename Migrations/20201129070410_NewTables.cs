﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LegalHunter.Migrations
{
    public partial class NewTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeedTbl",
                columns: table => new
                {
                    FeedID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeedType = table.Column<string>(nullable: true),
                    FeedTitle = table.Column<string>(nullable: true),
                    FeedImageUrl = table.Column<string>(nullable: true),
                    FeedVideoUrl = table.Column<string>(nullable: true),
                    FeedDocumentUrl = table.Column<string>(nullable: true),
                    FeedDesc = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedTbl", x => x.FeedID);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTbl",
                columns: table => new
                {
                    NotificationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NotificationTitle = table.Column<string>(nullable: true),
                    NotificationImageUrl = table.Column<string>(nullable: true),
                    NotificationDesc = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTbl", x => x.NotificationID);
                });

            migrationBuilder.CreateTable(
                name: "SubjectsTbl",
                columns: table => new
                {
                    SubjectsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubjectsName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectsTbl", x => x.SubjectsID);
                });

            migrationBuilder.CreateTable(
                name: "TrainingVideosTbl",
                columns: table => new
                {
                    TrainingVideosID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TrainingVideosTitle = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    VideoUrl = table.Column<string>(nullable: true),
                    TrainingVideosDesc = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingVideosTbl", x => x.TrainingVideosID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeedTbl");

            migrationBuilder.DropTable(
                name: "NotificationTbl");

            migrationBuilder.DropTable(
                name: "SubjectsTbl");

            migrationBuilder.DropTable(
                name: "TrainingVideosTbl");
        }
    }
}
