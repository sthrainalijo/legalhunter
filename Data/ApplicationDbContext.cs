﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LegalHunter.Data
{
    public class ApplicationUser : IdentityUser
    {

        public bool IsActive { get; set; }
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public string UserType { get; set; }
        public string ProfileImageSrc { get; set; }
        public string SubscriptionVersion { get; set; }
        public int CompanyID { get; set; }
        public int BranchID { get; set; }
    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Category> CategoryTbl { get; set; }
        public DbSet<Exam> ExamTbl { get; set; }
        public DbSet<ExamQuestionsMap> ExamQuestionsMapTbl { get; set; }
        public DbSet<Company> CompanyTbl { get; set; }
        public DbSet<Branch> BranchTbl { get; set; }
        public DbSet<Notification> NotificationTbl { get; set; }
        public DbSet<Feed> FeedTbl { get; set; }
        public DbSet<TrainingVideos> TrainingVideosTbl { get; set; }
        public DbSet<Subjects> SubjectsTbl { get; set; }
    }

    [Table("CategoryTbl")]
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        public int ParentCategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDesc { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
    [Table("ExamTbl")]
    public class Exam
    {
        [Key]
        public int ExamID { get; set; }
        public string ExamName { get; set; }
        public int ExamCategoryIDf { get; set; }
        public TimeSpan ExamDuration { get; set; }
        public string ExamDesc { get; set; }
        public DateTime ExamCreatedOn { get; set; }
        public string ExamCreatedBy { get; set; }
        public int ExamCreatedByBranchID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
    [Table("ExamQuestionsMapTbl")]
    public class ExamQuestionsMap
    {
        [Key]
        public int ExamQuestionID { get; set; }
        public int ExamQuestionNo{ get; set; }
        public int ExamIDf { get; set; }
        public string QuestionTitle { get; set; }
        public string QuestionDesc { get; set; }
        public string OptionA { get; set; }
        public bool IsOptionANeedImg { get; set; }
        public string OptionAImg { get; set; }
        public string OptionB { get; set; }
        public bool IsOptionBNeedImg { get; set; }
        public string OptionBImg { get; set; }
        public string OptionC { get; set; }
        public bool IsOptionCNeedImg { get; set; }
        public string OptionCImg { get; set; }
        public string OptionD { get; set; }
        public bool IsOptionDNeedImg { get; set; }
        public string OptionDImg { get; set; }
        public string CorrectOption { get; set; } 
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    [Table("CompanyTbl")]
    public class Company
    {
        [Key]
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string TaxRegisterationNumber { get; set; }
        public string Pincode { get; set; }
        public string CountryCode { get; set; }
        public string DefaultCurrency { get; set; }
        public string InvoiceDueTerms { get; set; }
        public string DefaultCurrencySymbol { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    [Table("BranchTbl")]
    public class Branch
    {
        [Key]
        public int BranchID { get; set; }
        public int CompanyIDf { get; set; }
        public string BranchName { get; set; }
        public string BranchDesc { get; set; }
        public string BranchEmail { get; set; }
        public string BranchContactNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    [Table("NotificationTbl")]
    public class Notification
    {
        [Key]
        public int NotificationID { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationImageUrl { get; set; }
        public string NotificationDesc { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    [Table("FeedTbl")]
    public class Feed
    {
        [Key]
        public int FeedID { get; set; }
        public string FeedType { get; set; }
        public string FeedTitle { get; set; }
        public string FeedImageUrl { get; set; }
        public string FeedVideoUrl { get; set; }
        public string FeedDocumentUrl { get; set; }
        public string FeedDesc { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    [Table("SubjectsTbl")]
    public class Subjects
    {
        [Key]
        public int SubjectsID { get; set; }
        public string SubjectsName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }

    [Table("TrainingVideosTbl")]
    public class TrainingVideos
    {
        [Key]
        public int TrainingVideosID { get; set; }
        public string TrainingVideosTitle { get; set; }
        public string ImageUrl { get; set; }
        public string VideoUrl { get; set; }
        public string TrainingVideosDesc { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
