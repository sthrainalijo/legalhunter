#pragma checksum "G:\legalHunter\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ca487e2cd6ce4b14bf0d947042445e79b1b89615"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "G:\legalHunter\Views\_ViewImports.cshtml"
using LegalHunter;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\legalHunter\Views\_ViewImports.cshtml"
using LegalHunter.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ca487e2cd6ce4b14bf0d947042445e79b1b89615", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8ad97e02e5e1a323d5369a384e3b073c5b8406d0", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "G:\legalHunter\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<!-- start head bottom -->
<div class=""bottom"">
    <div class=""left"">
        <h1>Dashboard</h1>
    </div>
    <div class=""right"">
        <h1>dashboard /</h1>
        <a href=""#"">Home</a>
    </div>
</div>
<!-- end head bottom -->
<div class=""row"">

    <!-- start analytics -->
    <div class=""col-lg-3"">
        <div class=""analytics"">
            <div class=""card"">
                <div class=""icon""><i class=""fa fa-video""></i></div>
                <div class=""text"">
                    <h1>984</h1>
                    <p>Total Exams</p>
                </div>
            </div>
        </div>
    </div>
    <div class=""col-lg-3"">
        <div class=""analytics"">
            <div class=""card"">
                <div class=""icon""><i class=""fab fa-vimeo-v""></i></div>
                <div class=""text"">
                    <h1>1455</h1>
                    <p>Total Questions</p>
                </div>
            </div>
        </div>
    </div>
    <div class=""col-lg-3"">
    ");
            WriteLiteral(@"    <div class=""analytics"">
            <div class=""card"">
                <div class=""icon""><i class=""fa fa-users""></i></div>
                <div class=""text"">
                    <h1>32</h1>
                    <p>Total users</p>
                </div>
            </div>
        </div>
    </div>
    <div class=""col-lg-3"">
        <div class=""analytics"">
            <div class=""card"">
                <div class=""icon""><i class=""fa fa-envelope""></i></div>
                <div class=""text"">
                    <h1>43</h1>
                    <p>Active Users</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end analytics -->
    <!-- start Active Leads -->
    <div class=""col-lg-7"">
        <div id=""leads"">
            <div class=""card"">
                <h1 class=""head"">Active Users</h1>
                <table class=""table"">
                    <!-- start head -->
                    <thead>
                        <tr>
                            ");
            WriteLiteral(@"<th>type</th>
                            <th>lead number</th>
                            <th>views</th>
                            <th>Favorites</th>
                            <th>Visit</th>
                            <th>Last Action</th>
                        </tr>
                    </thead>
                    <!-- end head -->
                    <!-- start body -->
                    <tbody>
                        <!-- start rows -->
                        <tr>
                            <td class=""text-primary"">Seller</td>
                            <td>mohamed said</td>
                            <td>532</td>
                            <td>864</td>
                            <td>12:23 AM</td>
                            <td><span class=""table-icon fa fa-envelope""></span> 42/6/2018</td>
                        </tr>
                        <tr>
                            <td class=""text-info"">Buyer</td>
                            <td>Denise Ann</td>
            ");
            WriteLiteral(@"                <td>150</td>
                            <td>150</td>
                            <td>9:23 AM</td>
                            <td><span class=""table-icon fa fa-envelope""></span> 11/9/2015</td>
                        </tr>
                        <tr>
                            <td class=""text-info"">Buyer</td>
                            <td>Denise Ann</td>
                            <td>150</td>
                            <td>150</td>
                            <td>9:23 AM</td>
                            <td><span class=""table-icon fa fa-envelope""></span> 11/9/2015</td>
                        </tr>
                        <tr>
                            <td class=""text-primary"">Seller</td>
                            <td>mohamed said</td>
                            <td>532</td>
                            <td>864</td>
                            <td>12:23 AM</td>
                            <td><span class=""table-icon fa fa-envelope""></span> 42/6/2018</td>
       ");
            WriteLiteral(@"                 </tr>
                        <tr>
                            <td class=""text-primary"">Seller</td>
                            <td>mohamed said</td>
                            <td>532</td>
                            <td>864</td>
                            <td>12:23 AM</td>
                            <td><span class=""table-icon fa fa-envelope""></span> 42/6/2018</td>
                        </tr>
                        <tr>
                            <td class=""text-info"">Buyer</td>
                            <td>Denise Ann</td>
                            <td>150</td>
                            <td>150</td>
                            <td>9:23 AM</td>
                            <td><span class=""table-icon fa fa-envelope""></span> 11/9/2015</td>
                        </tr>
                        <!-- end rows -->
                    </tbody>
                    <!-- end body -->
                </table>
            </div>
        </div>
    </div>
    <!-- en");
            WriteLiteral(@"d Active Leads -->
    <!-- start task card -->
    <div class=""col-lg-5"">
        <div id=""active"">
            <div class=""card"">
                <p class=""head"">Active user right now</p>
                <div class=""info"">
                    <div class=""col"">
                        <h1>937</h1>
                        <p>users</p>
                    </div>
                    <div class=""col"">
                        <h1>82</h1>
                        <p>guests</p>
                    </div>
                </div>
                <p class=""head"">Page view per aria</p>
                <div class=""aria"">
                    <p>22 from the United States of America</p>
                    <p>96 from the egypt</p>
                    <p>667 from the canada</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end task card -->

</div>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
