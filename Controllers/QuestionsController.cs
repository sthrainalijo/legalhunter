﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Dapper;
using LegalHunter.Data;
using LegalHunter.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Drawing;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LegalHunter.Controllers
{
    [Authorize(Roles = "PlatformOwner")]
    public class QuestionsController : Controller
    {
        readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public QuestionsController(IHostingEnvironment env, IMapper mapper, ApplicationDbContext Context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> SignInManager, IConfiguration configuration)
        {
            _env = env;
            db = Context;
            _userManager = userManager;
            _signInManager = SignInManager;
            _mapper = mapper;
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public IActionResult Index()
        {
            return View();
        }

        #region Questions
        [Authorize(Roles = "PlatformOwner")]
        public IActionResult Question(int ExamQuestionID, string Option,int? ExamIDf,string FromPage)
        {
            var CorrectOptionList = new List<SelectListItem> {
            new SelectListItem() { Value = "OptionA", Text = "Option A" },
            new SelectListItem() { Value = "OptionB", Text = "Option B" },
            new SelectListItem() { Value = "OptionC", Text = "Option C" },
            new SelectListItem() { Value = "OptionD", Text = "Option D" }
            };
            ViewBag.CorrectOptionList = CorrectOptionList;
            ViewBag.ExamList = db.ExamTbl.Where(x=>x.IsActive==true).ToList();
            QuestionViewModel Question = new QuestionViewModel();
            try
            {
                if (Option == "Edit")
                {
                    var QuestionDetails = db.ExamQuestionsMapTbl.Where(x => x.ExamQuestionID == ExamQuestionID).FirstOrDefault();
                    Question = _mapper.Map<QuestionViewModel>(QuestionDetails);
                    Question.Option = "Edit";
                }
                else if(ExamIDf!=null && ExamIDf != 0)
                {
                    var ExamIDfWithoutNull = Convert.ToInt32(ExamIDf);
                    Question.ExamIDf = ExamIDfWithoutNull;
                }
                if(FromPage=="Exam")
                {
                    Question.FromPage = FromPage;
                }

            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - LegalHunter");
            }
            return View(Question);
        }

        [Authorize(Roles = "PlatformOwner")]
        [HttpPost]
        public async Task<IActionResult> Question(QuestionViewModel PostObject)
        {
            var CorrectOptionList = new List<SelectListItem> {
            new SelectListItem() { Value = "OptionA", Text = "Option A" },
            new SelectListItem() { Value = "OptionB", Text = "Option B" },
            new SelectListItem() { Value = "OptionC", Text = "Option C" },
            new SelectListItem() { Value = "OptionD", Text = "Option D" }
            };
            ViewBag.CorrectOptionList = CorrectOptionList;
            ViewBag.ExamList = db.ExamTbl.Where(x => x.IsActive == true).ToList();
            try
            {
                int ExamQuestionIDtoUpdate;
                if (PostObject.Option == "Edit")
                {
                    ExamQuestionIDtoUpdate = PostObject.ExamQuestionID;
                }
                else
                {
                    var LastExamQuestionNo = db.ExamQuestionsMapTbl.Where(x => x.IsActive == true&&x.ExamIDf==PostObject.ExamIDf).OrderByDescending(x => x.ExamQuestionNo).Select(x => x.ExamQuestionNo).FirstOrDefault();
                    int? ExamQuestionNo = 0;
                    if (LastExamQuestionNo != null)
                    {
                        ExamQuestionNo = LastExamQuestionNo + 1;
                    }
                    else
                    {
                        ExamQuestionNo = 1;
                    }
                    ExamQuestionsMap NewObject = new ExamQuestionsMap();
                    NewObject.IsActive = true;
                    NewObject.IsDelete = false;
                    NewObject.ExamQuestionNo = Convert.ToInt32(ExamQuestionNo);
                    db.ExamQuestionsMapTbl.Add(NewObject);
                    db.SaveChanges();
                    ExamQuestionIDtoUpdate = NewObject.ExamQuestionID;
                }
                var CurrentDate = Common.GetCurrentDateTime();
                var ExamQuestionUpdate = db.ExamQuestionsMapTbl.Where(x => x.ExamQuestionID == ExamQuestionIDtoUpdate).FirstOrDefault();
                ExamQuestionUpdate.QuestionTitle = PostObject.QuestionTitle;
                ExamQuestionUpdate.QuestionDesc = PostObject.QuestionDesc;
                ExamQuestionUpdate.ExamIDf = PostObject.ExamIDf;
                ExamQuestionUpdate.OptionA = PostObject.OptionA;
                ExamQuestionUpdate.OptionB = PostObject.OptionB;
                ExamQuestionUpdate.OptionC = PostObject.OptionC;
                ExamQuestionUpdate.OptionD = PostObject.OptionD;
                ExamQuestionUpdate.CorrectOption = PostObject.CorrectOption;
                ExamQuestionUpdate.IsOptionANeedImg = PostObject.IsOptionANeedImg;
                ExamQuestionUpdate.IsOptionBNeedImg = PostObject.IsOptionBNeedImg;
                ExamQuestionUpdate.IsOptionCNeedImg = PostObject.IsOptionCNeedImg;
                ExamQuestionUpdate.IsOptionDNeedImg = PostObject.IsOptionDNeedImg;

                if (PostObject.FinalOptionASrcID != null && PostObject.FinalOptionASrcID != "" && PostObject.FinalOptionASrcID != "Exists")
                {
                    var RelativePath = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                    var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePath);
                    var OptionAImgFileName = "";
                    if (PostObject.OptionAImageFile != null)
                    {
                        OptionAImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionAImg-id" + Common.generateID() + Path.GetExtension(PostObject.OptionAImageFile.FileName);
                    }
                    else
                    {
                        OptionAImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionAImg-id" + Common.generateID() + ".jpeg";
                    }

                    if (!Directory.Exists(PathWithFolderName))
                        Directory.CreateDirectory(PathWithFolderName);

                    var filePath = Path.Combine(PathWithFolderName, OptionAImgFileName);

                    PostObject.FinalOptionASrcID = PostObject.FinalOptionASrcID.Replace("data:image/png;base64,", "");

                    var OldFilePathWithFolderName = Path.Combine(_env.WebRootPath, (PostObject.OptionAImg ?? ""));

                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(PostObject.FinalOptionASrcID)))
                    {
                        using (Bitmap bm2 = new Bitmap(ms))
                        {
                            bm2.Save(filePath);

                            if (System.IO.File.Exists(OldFilePathWithFolderName))
                            {
                                System.IO.File.Delete(OldFilePathWithFolderName);
                            }

                            ExamQuestionUpdate.OptionAImg = RelativePath + "/" + OptionAImgFileName;
                        }
                    }


                }

                if (PostObject.FinalOptionBSrcID != null && PostObject.FinalOptionBSrcID != "" && PostObject.FinalOptionBSrcID != "Exists")
                {
                    var RelativePath = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                    var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePath);
                    var OptionBImgFileName = "";
                    if (PostObject.OptionBImageFile != null)
                    {
                        OptionBImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionBImg-id" + Common.generateID() + Path.GetExtension(PostObject.OptionBImageFile.FileName);
                    }
                    else
                    {
                        OptionBImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionBImg-id" + Common.generateID() + ".jpeg";
                    }

                    if (!Directory.Exists(PathWithFolderName))
                        Directory.CreateDirectory(PathWithFolderName);

                    var filePath = Path.Combine(PathWithFolderName, OptionBImgFileName);

                    PostObject.FinalOptionBSrcID = PostObject.FinalOptionBSrcID.Replace("data:image/png;base64,", "");

                    var OldFilePathWithFolderName = Path.Combine(_env.WebRootPath, (PostObject.OptionBImg ?? ""));

                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(PostObject.FinalOptionBSrcID)))
                    {
                        using (Bitmap bm2 = new Bitmap(ms))
                        {
                            bm2.Save(filePath);

                            if (System.IO.File.Exists(OldFilePathWithFolderName))
                            {
                                System.IO.File.Delete(OldFilePathWithFolderName);
                            }

                            ExamQuestionUpdate.OptionBImg = RelativePath + "/" + OptionBImgFileName;
                        }
                    }


                }

                if (PostObject.FinalOptionCSrcID != null && PostObject.FinalOptionCSrcID != "" && PostObject.FinalOptionCSrcID != "Exists")
                {
                    var RelativePath = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                    var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePath);
                    var OptionCImgFileName = "";
                    if (PostObject.OptionCImageFile != null)
                    {
                        OptionCImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionCImg-id" + Common.generateID() + Path.GetExtension(PostObject.OptionCImageFile.FileName);
                    }
                    else
                    {
                        OptionCImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionCImg-id" + Common.generateID() + ".jpeg";
                    }

                    if (!Directory.Exists(PathWithFolderName))
                        Directory.CreateDirectory(PathWithFolderName);

                    var filePath = Path.Combine(PathWithFolderName, OptionCImgFileName);

                    PostObject.FinalOptionCSrcID = PostObject.FinalOptionCSrcID.Replace("data:image/png;base64,", "");

                    var OldFilePathWithFolderName = Path.Combine(_env.WebRootPath, (PostObject.OptionCImg ?? ""));

                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(PostObject.FinalOptionCSrcID)))
                    {
                        using (Bitmap bm2 = new Bitmap(ms))
                        {
                            bm2.Save(filePath);

                            if (System.IO.File.Exists(OldFilePathWithFolderName))
                            {
                                System.IO.File.Delete(OldFilePathWithFolderName);
                            }

                            ExamQuestionUpdate.OptionCImg = RelativePath + "/" + OptionCImgFileName;
                        }
                    }


                }

                if (PostObject.FinalOptionDSrcID != null && PostObject.FinalOptionDSrcID != "" && PostObject.FinalOptionDSrcID != "Exists")
                {
                    var RelativePath = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                    var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePath);
                    var OptionDImgFileName = "";
                    if (PostObject.OptionDImageFile != null)
                    {
                        OptionDImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionDImg-id" + Common.generateID() + Path.GetExtension(PostObject.OptionDImageFile.FileName);
                    }
                    else
                    {
                        OptionDImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionDImg-id" + Common.generateID() + ".jpeg";
                    }

                    if (!Directory.Exists(PathWithFolderName))
                        Directory.CreateDirectory(PathWithFolderName);

                    var filePath = Path.Combine(PathWithFolderName, OptionDImgFileName);

                    PostObject.FinalOptionDSrcID = PostObject.FinalOptionDSrcID.Replace("data:image/png;base64,", "");

                    var OldFilePathWithFolderName = Path.Combine(_env.WebRootPath, (PostObject.OptionDImg ?? ""));

                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(PostObject.FinalOptionDSrcID)))
                    {
                        using (Bitmap bm2 = new Bitmap(ms))
                        {
                            bm2.Save(filePath);

                            if (System.IO.File.Exists(OldFilePathWithFolderName))
                            {
                                System.IO.File.Delete(OldFilePathWithFolderName);
                            }

                            ExamQuestionUpdate.OptionDImg = RelativePath + "/" + OptionDImgFileName;
                        }
                    }


                }


                db.SaveChanges();
                TempData["AlertStatus"] += Common.GenerateNotification("Question Updated Successfully", "success");
                if(PostObject.FromPage=="Exam")
                {
                    return RedirectToAction("Exam","Exams",new { ExamID=PostObject.ExamIDf ,Option = "Edit" });
                }
                else
                {
                    return RedirectToAction("QuestionsList");
                }
                

            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - LegalHunter");
                TempData["AlertStatus"] += Common.GenerateNotification("Question Update-Error", "danger");
                return View(PostObject);
            }
        }

 
        [Authorize(Roles = "PlatformOwner")]
        public IActionResult QuestionsList()
        {
            return View();
        }


        [AllowAnonymous]
        public string SearchQuestion()
        {
            string limit, start, searchKey, orderColumn, orderDir, draw, jsonString, filterType;
            limit = HttpContext.Request.Form["length"].FirstOrDefault();
            start = HttpContext.Request.Form["start"].FirstOrDefault();
            searchKey = HttpContext.Request.Form["search[value]"].FirstOrDefault();
            orderColumn = HttpContext.Request.Form["order[0][column]"].FirstOrDefault();
            orderDir = HttpContext.Request.Form["order[0][dir]"].FirstOrDefault();
            draw = HttpContext.Request.Form["draw"].FirstOrDefault();

            string ExamID = HttpContext.Request.Form["ExamID"].FirstOrDefault();
            if(ExamID==null)
            {
                ExamID = "";
            }
            using (IDbConnection conn = new SqlConnection(_connectionString))
            {

                var reader = conn.QueryMultiple("SearchQuestion", param: new { orderColumn = orderColumn, limit = limit, orderDir = orderDir, start = start, searchKey = searchKey, ExamID= ExamID }, commandType: CommandType.StoredProcedure);

                var ExamQuestionSearchList = reader.Read<QuestionSearchModel>().ToList();

                int count = 0;
                if (ExamQuestionSearchList.Count > 0)
                {
                    count = ExamQuestionSearchList.FirstOrDefault().TotalCount;
                }
                dynamic newtonresult = new
                {
                    status = "success",
                    draw = Convert.ToInt32(draw == "" ? "0" : draw),
                    recordsTotal = count,
                    recordsFiltered = count,
                    data = ExamQuestionSearchList
                };
                jsonString = JsonConvert.SerializeObject(newtonresult);

                return jsonString;
            }

        }

        public class QuestionSearchModel : QuestionViewModel
        {
            public int TotalCount { get; set; }
        }

        [Authorize(Roles = "PlatformOwner")]
        public ActionResult QuestionStatus(int id, bool Status,string FromPage)
        {
            try
            {


                var ExamQuestionDetails = db.ExamQuestionsMapTbl.Where(x => x.ExamQuestionID == id).FirstOrDefault();

                if (ExamQuestionDetails != null)
                {
                    //db.RawMaterialTbls.Remove(CheckRawMaterial); USED FOR DELETE FROM PROJECT
                    ExamQuestionDetails.IsActive = Status;
                    db.SaveChanges();
                    TempData["AlertStatus"] += Common.GenerateNotification("Question Status Updated Successfully", "warning");
                    if (FromPage == "Exam")
                    {
                        return RedirectToAction("Exam", "Exams", new { ExamID = ExamQuestionDetails.ExamIDf, Option = "Edit" });
                    }
                    else
                    {
                        return RedirectToAction("QuestionsList");
                    }
                }
                else
                {
                    TempData["AlertStatus"] += Common.GenerateNotification("Question Status Update-Error", "danger");
                    if (FromPage == "Exam")
                    {
                        return RedirectToAction("Exam", "Exams", new { ExamID = ExamQuestionDetails.ExamIDf, Option = "Edit" });
                    }
                    else
                    {
                        return RedirectToAction("QuestionsList");
                    }
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - Legal Hunter");
                return RedirectToAction("QuestionsList");
            }


        }
        [Authorize(Roles = "PlatformOwner")]
        public IActionResult DeleteQuestion(int id,string FromPage)
        {
            var ExamQuestionDetails = db.ExamQuestionsMapTbl.Where(x => x.ExamQuestionID == id).FirstOrDefault();
            try
            {

                if (ExamQuestionDetails != null)
                {
                    //db.RawMaterialTbls.Remove(CheckRawMaterial); USED FOR DELETE FROM PROJECT
                    ExamQuestionDetails.IsActive = false;
                    ExamQuestionDetails.IsDelete = true;

                    //Rearrange Exam Questions
                    var AllQuestionNo = db.ExamQuestionsMapTbl.Where(x => x.ExamIDf == ExamQuestionDetails.ExamIDf).OrderBy(x=>x.ExamQuestionNo).ToList();
                    int Count = 1;
                    foreach(var Item in AllQuestionNo)
                    {
                        Item.ExamQuestionNo = Count;
                        Count++;
                    }

                    db.SaveChanges();
                    TempData["AlertStatus"] += Common.GenerateNotification("Question Status Deleted Successfully", "warning");
                    if (FromPage == "Exam")
                    {
                        return RedirectToAction("Exam", "Exams", new { ExamID = ExamQuestionDetails.ExamIDf, Option = "Edit" });
                    }
                    else
                    {
                        return RedirectToAction("QuestionsList");
                    }
                }
                else
                {
                    TempData["AlertStatus"] += Common.GenerateNotification("Question Status Delete-Error", "danger");
                    if (FromPage == "Exam")
                    {
                        return RedirectToAction("Exam", "Exams", new { ExamID = ExamQuestionDetails.ExamIDf, Option = "Edit" });
                    }
                    else
                    {
                        return RedirectToAction("QuestionsList");
                    }
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - Legal Hunter");
                if (FromPage == "Exam")
                {
                    return RedirectToAction("Exam", "Exams", new { ExamID = ExamQuestionDetails.ExamIDf, Option = "Edit" });
                }
                else
                {
                    return RedirectToAction("QuestionsList");
                }
            }
        }
        #endregion

        public string GetUserID()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            return userId;
        }

        #region Helpers
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();

                }

            }

            base.Dispose(disposing);
        }

        #endregion
    }
}
