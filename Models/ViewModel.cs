﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LegalHunter.Models
{
    public class ViewModel
    {
    }
    public class ExamViewModel
    {

        public int ExamID { get; set; }
        [Required(ErrorMessage = "Exam Name Is Required")]
        public string ExamName { get; set; }
        [Required(ErrorMessage = "Exam Category Is Required")]
        public int ExamCategoryIDf { get; set; }
        [Required(ErrorMessage = "Exam Duration Is Required")]
        public TimeSpan ExamDuration { get; set; }
        public string ExamDesc { get; set; }
        public DateTime ExamCreatedOn { get; set; }
        public string ExamCreatedBy { get; set; }
        public string ExamCreatedByBranchID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Option { get; set; }

    }

    public class QuestionViewModel
    {

        public int ExamQuestionID { get; set; }
        [Required(ErrorMessage = "Please Select Exam")]
        public int ExamIDf { get; set; }
        [Required(ErrorMessage = "Question Title Is Required")]
        public string QuestionTitle { get; set; }
        public string QuestionDesc { get; set; }
        [Required(ErrorMessage = "Option A Is Required")]
        public string OptionA { get; set; }
        public bool IsOptionANeedImg { get; set; }
        public string OptionAImg { get; set; }
        [Required(ErrorMessage = "Option B Is Required")]
        public string OptionB { get; set; }
        public bool IsOptionBNeedImg { get; set; }
        public string OptionBImg { get; set; }
        [Required(ErrorMessage = "Option C Is Required")]
        public string OptionC { get; set; }
        public bool IsOptionCNeedImg { get; set; }
        public string OptionCImg { get; set; }
        [Required(ErrorMessage = "Option D Is Required")]
        public string OptionD { get; set; }
        public bool IsOptionDNeedImg { get; set; }
        public string OptionDImg { get; set; }
        public string CorrectOption { get; set; }

        public IFormFile OptionAImageFile { get; set; }
        public IFormFile OptionBImageFile { get; set; }
        public IFormFile OptionCImageFile { get; set; }
        public IFormFile OptionDImageFile { get; set; }

        public string FinalOptionASrcID { get; set; }
        public string FinalOptionBSrcID { get; set; }
        public string FinalOptionCSrcID { get; set; }
        public string FinalOptionDSrcID { get; set; }

        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Option { get; set; }
        public string FromPage { get; set; }

    }
    public class CategoryViewModel
    {

        public int CategoryID { get; set; }
        public int ParentCategoryID { get; set; }
        [Required(ErrorMessage = "Category Name is required")]
        public string CategoryName { get; set; }
        public string CategoryDesc { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsParentCategory { get; set; }
        public string FullCategory { get; set; }
        public string Option { get; set; }
        public static string BuildCatStringList(string suffix, CategoryViewModel c,
                        IEnumerable<CategoryViewModel> categories, long ParentCategoryID)
        {
            if (ParentCategoryID == 0)
            {

            }
            else
            {
                if (suffix == "")
                {
                    suffix = c.CategoryName;
                }

                var parent = categories.Where(cats => cats.CategoryID == ParentCategoryID).FirstOrDefault();
                if (parent == null)
                {
                    return c.FullCategory;
                }
                else
                {
                    // return c.FullCategory;
                    c.FullCategory = parent.CategoryName + " / " + suffix;
                    BuildCatStringList(parent.CategoryName + " / " + suffix,
                                       c, categories, parent.ParentCategoryID);
                }
            }
            return c.FullCategory;
        }

    }
}
